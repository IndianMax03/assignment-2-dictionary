section     .data
%include    "main_lib.inc"
%include    "words.inc"
%define     PSIZE 8
%define     BSIZE 255
%define     WRITE 1
%define     ERR 2

section     .bss
buffer:     resb    BSIZE

section     .rodata
fatal_reading_msg:  db  "Reading error", 0
fatal_key_msg:      db  "Not found", 0

section     .text
global      _start

_start:
    mov     rdi, buffer                             ;  buffer start pointer
    mov     rsi, BSIZE                              ;  buffer size
    call    read_word                               ;  {0, buffer address} -> rax, str_len -> rdx
    test    rax, rax                                ;  if (rax == 0) {ZF = 0} <=> can't read
    jz      fatal_reading                           ;  
    push    rdx                                     ;  caller-saved str_len
    mov     rdi, rax                                ;  rdi -- n/t string pointer
    mov     rsi, LINK                               ;  rsi -- dict pointer
    call    find_word                               ;  rax = {0, pointer}
    pop     rdx                                     ;  ret str_len
    test    rax, rax                                ;  if (rax == 0) {ZF = 0} <=> key not found
    jz      fatal_key                               ;  
success:
    lea     rdi, [(rax) + (1 * PSIZE) + (rdx + 1)]  ;  [base + scale + offset]
    jmp     log                                     ;  all is OK => go to print (stdout)

fatal_key:
    mov     rdi, fatal_key_msg
    jmp     err                                     ;  go to print (stderr)

fatal_reading:
    mov     rdi, fatal_reading_msg

err:
    mov rsi, rdi ; указатель на начало строки -> rsi - адрес начала строки-источника
    push rsi ; caller saved -- saving
    call string_length ; длина строки -> rax 
    pop rsi ; caller saved -- recovering
    mov rdx, rax ; записали длину строки для записи
    mov rax, WRITE ; номер системного вызова write
    mov rdi, ERR ; в stderr
    syscall ; системный вызов
    jmp exit

log:
    call    print_string
    call    print_newline
    jmp    exit
