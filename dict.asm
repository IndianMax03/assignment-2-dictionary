section     .data
%include    "dict_lib.inc"
%define     PSIZE 8
%define     NOT_FOUND 0

section     .text
global      find_word

;  rdi -- n/t string pointer, rsi -- dict pointer
;  ret -- rax = {0, pointer}
find_word:
    .iteration:
        push    rdi                        ;  caller-saved
        push    rsi                        ;  caller-saved
        add     rsi, PSIZE                 ;  move to the next element
        call    string_equals              ;  1/0 -> rax
        pop     rsi                        ;  ret
        pop     rdi                        ;  ret
        test    rax, rax                   ;  if (rax == 0) {ZF = 0}
        jnz     .success                   ;  <=> jmp if strings equals
        mov     rsi, [rsi]                 ;  iterate
        test    rsi, rsi                   ;  if (rsi == 0) {ZF = 0}
        jnz     .iteration                 ;  while (!n/t)
    .fatal:                                ;  n/t and !success
        mov     rax, NOT_FOUND
        ret
    .success:                              ; success: rsi holds pointer
        mov     rax, rsi
        ret
