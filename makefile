ASM=nasm
ASMFLAGS=-f elf64

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

run: main.o dict.o lib.o
	ld -o $@ $^

.PHONY: clean

clean:
	rm *.o
	rm run
